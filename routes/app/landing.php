<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Landing\MainController;

Route::group(['domain' => ''], function() {
    Route::prefix('')->name('web.')->group(function(){
        Route::get('',[MainController::class, 'index'])->name('home');
    });
});