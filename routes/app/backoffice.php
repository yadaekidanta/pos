<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backoffice\AuthController;
use App\Http\Controllers\Backoffice\DashboardController;
use App\Http\Controllers\Backoffice\Library\TaxController;
use App\Http\Controllers\Backoffice\Library\BrandController;
use App\Http\Controllers\Backoffice\Library\PromoController;
use App\Http\Controllers\Backoffice\Library\DiscountController;
use App\Http\Controllers\Backoffice\Library\GratuityController;
use App\Http\Controllers\Backoffice\Library\ModifierController;
use App\Http\Controllers\Backoffice\Customer\CustomerController;
use App\Http\Controllers\Backoffice\Customer\FeedbackController;
use App\Http\Controllers\Backoffice\Library\SalesTypeController;
use App\Http\Controllers\Backoffice\Inventory\SupplierController;
use App\Http\Controllers\Backoffice\Inventory\TransferController;
use App\Http\Controllers\Backoffice\Library\ItemLibraryController;
use App\Http\Controllers\Backoffice\Inventory\AdjustmentController;
use App\Http\Controllers\Backoffice\Library\ItemCategoryController;
use App\Http\Controllers\Backoffice\Customer\LoyaltyProgramController;
use App\Http\Controllers\Backoffice\Inventory\PurchaseOrderController;
use App\Http\Controllers\Backoffice\TableManagement\TableMapController;
use App\Http\Controllers\Backoffice\TableManagement\TableGroupController;

Route::group(['domain' => ''], function () {
    Route::prefix('backoffice')->name('backoffice.')->group(function () {
        // Route::get('lang/{language}',[MainController::class, 'switch'])->name('switch.lang');
        Route::prefix('auth')->name('auth.')->group(function () {
            Route::get('', [AuthController::class, 'index'])->name('index');
            Route::post('login', [AuthController::class, 'do_login'])->name('login');
            Route::get('register', [AuthController::class, 'register'])->name('register');
            Route::post('register', [AuthController::class, 'do_register'])->name('do_register');
            Route::get('forgot', [AuthController::class, 'forgot'])->name('forgot');
            Route::post('forgot', [AuthController::class, 'do_forgot'])->name('do_forgot');
        });
        // Route::middleware(['auth:office','verified'])->group(function(){
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
        // LIBRARY
        Route::prefix('library')->name('library.')->group(function () {
            Route::resource('item-library', ItemLibraryController::class);
            Route::resource('modifier', ModifierController::class);
            Route::resource('categories', ItemCategoryController::class);
            Route::get('categories/{category}/assign', [ItemCategoryController::class, 'updateStatus'])->name('categories.assign.view');
            Route::patch('categories/{category}/assign', [ItemCategoryController::class, 'assign'])->name('categories.assign');
            Route::resource('promo', PromoController::class);
            Route::resource('discounts', DiscountController::class);
            Route::resource('taxes', TaxController::class);
            Route::resource('gratuity', GratuityController::class);
            Route::resource('sales-type', SalesTypeController::class);
            Route::resource('brands', BrandController::class);
        });
        // INVENTORY
        Route::prefix('inventory')->name('inventory.')->group(function () {
            Route::resource('supplier', SupplierController::class);
            Route::resource('purchase-order', PurchaseOrderController::class);
            Route::resource('transfer', TransferController::class);
            Route::resource('adjustment', AdjustmentController::class);
        });
        // CUSTOMER
        Route::prefix('customer')->name('customer.')->group(function () {
            Route::resource('list', CustomerController::class);
            Route::resource('feedback', FeedbackController::class);
            Route::resource('loyalty-program', LoyaltyProgramController::class);
        });
        // TABLE MANAGEMENT
        Route::prefix('table-management')->name('table-management.')->group(function () {
            Route::resource('table-group', TableGroupController::class);
            Route::resource('table-map', TableMapController::class);
            Route::post('table-group/{table_group}/duplicate', [TableGroupController::class, 'duplicate'])->name('table-group.duplicate');
        });
        Route::get('logout', [AuthController::class, 'do_logout'])->name('auth.logout');
        // });
    });
});
