<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/';
    public const BACKOFFICE = '/backoffice/dashboard';
    public const OFFICE = '/office/dashboard';
    public const POS = '/pos/transaction';
    public function boot()
    {
        $this->configureRateLimiting();
        $this->routes(function () {
            Route::prefix('api')->middleware('api')->namespace($this->namespace)->group(base_path('routes/app/api.php'));
            Route::middleware('web')->namespace($this->namespace)->group(base_path('routes/app/landing.php'));
            Route::middleware('backoffice')->namespace($this->namespace)->group(base_path('routes/app/backoffice.php'));
            Route::middleware('office')->namespace($this->namespace)->group(base_path('routes/app/office.php'));
            Route::middleware('pos')->namespace($this->namespace)->group(base_path('routes/app/pos.php'));
        });
    }
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
