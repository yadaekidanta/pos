<?php

namespace App\Models\TableManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableMap extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function tableGroup()
    {
        return $this->belongsTo(TableGroup::class, 'table_group_id', 'id');
    }
}
