<?php

namespace App\Models\TableManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableGroup extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function tableMap()
    {
        return $this->hasMany(TableMap::class, 'table_group_id', 'id');
    }
}
