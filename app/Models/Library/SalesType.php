<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\AsCollection;
use Illuminate\Database\Eloquent\Model;

class SalesType extends Model
{
    use HasFactory;

    protected $table = 'sales_type';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $casts = [
        'gratuity_applied' => AsCollection::class,
    ];
}
