<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(){

    }
    public function index(){
        return view('pages.backoffice.auth.main');
    }
    public function do_login(){
        //
    }
    public function register(){
        return view('pages.backoffice.auth.register');
    }
    public function do_register(){
        //
    }
    public function do_verify(){
        //
    }
    public function forgot(){
        return view('pages.backoffice.auth.forgot');
    }
    public function do_forgot(){
        //
    }
    public function reset(){
        //
    }
    public function do_reset(){
        //
    }
    public function do_logout(){
        //
    }
}
