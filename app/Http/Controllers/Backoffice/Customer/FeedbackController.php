<?php

namespace App\Http\Controllers\Backoffice\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Feedback $feedback)
    {
        //
    }
    public function edit(Feedback $feedback)
    {
        //
    }
    public function update(Request $request, Feedback $feedback)
    {
        //
    }
    public function destroy(Feedback $feedback)
    {
        //
    }
}
