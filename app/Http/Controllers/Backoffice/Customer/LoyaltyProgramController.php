<?php

namespace App\Http\Controllers\Backoffice\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer\LoyaltyProgram;
use Illuminate\Http\Request;

class LoyaltyProgramController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(LoyaltyProgram $loyaltyProgram)
    {
        //
    }
    public function edit(LoyaltyProgram $loyaltyProgram)
    {
        //
    }
    public function update(Request $request, LoyaltyProgram $loyaltyProgram)
    {
        //
    }
    public function destroy(LoyaltyProgram $loyaltyProgram)
    {
        //
    }
}
