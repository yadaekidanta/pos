<?php

namespace App\Http\Controllers\Backoffice\TableManagement;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\TableManagement\TableMap;
use Illuminate\Support\Facades\Validator;
use App\Models\TableManagement\TableGroup;

class TableMapController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $table_group_id = $request->table_group_id ?? false ? $request->table_group_id : TableGroup::orderBy('id', 'desc')->get()[0]->id;
            $keyword = $request->keyword;
            $tableMaps = TableMap::with(['tableGroup'])->where('table_group_id', $table_group_id)->where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.table-management.map.list', [
                'tableMaps' => $tableMaps
            ]);
        }
        return view('pages.backoffice.table-management.map.main', [
            'tableGroups' => TableGroup::orderBy('id', 'desc')->get()
        ]);
    }
    public function create()
    {
        return view('pages.backoffice.table-management.map.input', [
            'tableGroups' => TableGroup::orderBy('id', 'desc')->get()
        ]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'table_group_id' => ['required', 'exists:table_groups,id'],
            'name' => ['required'],
            'pax' => ['required', 'numeric'],
            'shape' => ['required', Rule::in(['square', 'circle'])],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('table_group_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('table_group_id'),
                ]);
            }
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            if ($errors->has('pax')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pax'),
                ]);
            }
            if ($errors->has('shape')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('shape'),
                ]);
            }
        }
        TableMap::create([
            'table_group_id' => $request->table_group_id,
            'name' => $request->name,
            'pax' => $request->pax,
            'shape' => $request->shape
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Table added.',
        ]);
    }
    public function show(TableMap $tableMap)
    {
        //
    }
    public function edit(TableMap $tableMap)
    {
        return view('pages.backoffice.table-management.map.input', [
            'tableGroups' => TableGroup::orderBy('id', 'desc')->get(),
            'tableMap' => $tableMap
        ]);
    }
    public function update(Request $request, TableMap $tableMap)
    {
        $validator = Validator::make($request->all(), [
            'table_group_id' => ['required', 'exists:table_groups,id'],
            'name' => ['required'],
            'pax' => ['required', 'numeric'],
            'shape' => ['required', Rule::in(['square', 'circle'])],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('table_group_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('table_group_id'),
                ]);
            }
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            if ($errors->has('pax')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pax'),
                ]);
            }
            if ($errors->has('shape')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('shape'),
                ]);
            }
        }
        TableMap::where('id', $tableMap->id)->update([
            'table_group_id' => $request->table_group_id,
            'name' => $request->name,
            'pax' => $request->pax,
            'shape' => $request->shape
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Table updated.',
        ]);
    }
    public function destroy(TableMap $tableMap)
    {
        $tableMap->delete();
        // TableMap::destroy($tableMap->id);
        return response()->json([
            'alert' => 'success',
            'message' => 'Table deleted.',
        ]);
    }
}
