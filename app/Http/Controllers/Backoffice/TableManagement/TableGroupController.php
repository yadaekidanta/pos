<?php

namespace App\Http\Controllers\Backoffice\TableManagement;

use App\Http\Controllers\Controller;
use App\Models\TableManagement\TableGroup;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class TableGroupController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $tableGroups = TableGroup::with(['tableMap'])->where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.table-management.group.list', [
                'tableGroups' => $tableGroups
            ]);
        }
        return view('pages.backoffice.table-management.group.main');
    }
    public function create()
    {
        return view('pages.backoffice.table-management.group.input');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'status' => [Rule::in(['active'])],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            if ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status'),
                ]);
            }
        }
        TableGroup::create([
            'name' => $request->name,
            'status' => $request->status ?? false ? $request->status : 'inactive'
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Table Group created.',
        ]);
    }
    public function show(TableGroup $tableGroup)
    {
        //
    }
    public function edit(TableGroup $tableGroup)
    {
        return view('pages.backoffice.table-management.group.input', ['tableGroup' => $tableGroup]);
    }
    public function update(Request $request, TableGroup $tableGroup)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'status' => [Rule::in(['active'])],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            if ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status'),
                ]);
            }
        }
        TableGroup::where('id', $tableGroup->id)->update([
            'name' => $request->name,
            'status' => $request->status ?? false ? $request->status : 'inactive'
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Table Group updated.',
        ]);
    }
    public function destroy(TableGroup $tableGroup)
    {
        $tableGroup->delete();
        // TableGroup::destroy($tableGroup->id);
        return response()->json([
            'alert' => 'success',
            'message' => 'Table Group deleted.',
        ]);
    }
    public function duplicate(TableGroup $tableGroup)
    {
        $newTableGroup = TableGroup::create([
            'name' => $tableGroup->name . ' copy',
            'status' => 'inactive'
        ]);
        foreach ($tableGroup->tableMap as $tableMap) {
            $newTableMap = $tableMap->replicate();
            $newTableMap->table_group_id = $newTableGroup->id;
            $newTableMap->name = $tableMap->name . ' copy';
            $newTableMap->save();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Table Group duplicated.',
        ]);
    }
}
