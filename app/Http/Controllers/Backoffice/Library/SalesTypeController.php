<?php

namespace App\Http\Controllers\Backoffice\Library;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Library\Gratuity;
use App\Models\Library\SalesType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SalesTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $salesTypes = SalesType::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.sales-type.list', [
                'salesTypes' => $salesTypes
            ]);
        }
        return view('pages.backoffice.library.sales-type.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.sales-type.input', [
            'gratuities' => Gratuity::orderBy('id', 'desc')->get()
        ]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'unique:sales_type,name'],
            'status' => [Rule::in(['active'])],
            'gratuity_applied' => 'array',
            'gratuity_applied.*' => 'exists:gratuity,id'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status')
                ]);
            }
            if ($errors->has('gratuity_applied')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gratuity_applied')
                ]);
            }
            if ($errors->has('gratuity_applied.*')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gratuity_applied.*')
                ]);
            }
        }
        SalesType::create([
            'name' => $request->name,
            'status' => $request->status ?? false ? $request->status : 'inactive',
            'gratuity_applied' => $request->gratuity_applied ?? false ? array_map('intval', $request->gratuity_applied) : null
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Sales Type created.'
        ]);
    }
    public function show(SalesType $sales_type)
    {
        //
    }
    public function edit(SalesType $sales_type)
    {
        return view('pages.backoffice.library.sales-type.input', [
            'salesType' => $sales_type,
            'gratuities' => Gratuity::orderBy('id', 'desc')->get()
        ]);
    }
    public function update(Request $request, SalesType $sales_type)
    {
        $rules = [
            'name' => ['required'],
            'status' => [Rule::in(['active'])],
            'gratuity_applied' => 'array',
            'gratuity_applied.*' => 'exists:gratuity,id'
        ];
        if ($request->name != $sales_type->name) {
            $rules['name'] += [count($rules['name']) => 'unique:sales_type,name'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status')
                ]);
            }
            if ($errors->has('gratuity_applied')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gratuity_applied')
                ]);
            }
            if ($errors->has('gratuity_applied.*')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gratuity_applied.*')
                ]);
            }
        }
        SalesType::where('id', $sales_type->id)->update([
            'name' => $request->name,
            'status' => $request->status ?? false ? $request->status : 'inactive',
            'gratuity_applied' => $request->gratuity_applied ?? false ? array_map('intval', $request->gratuity_applied) : null
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Sales Type updated.'
        ]);
    }
    public function destroy(SalesType $sales_type)
    {
        $sales_type->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Sales Type deleted.'
        ]);
    }
}
