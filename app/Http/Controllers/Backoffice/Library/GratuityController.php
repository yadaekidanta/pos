<?php

namespace App\Http\Controllers\Backoffice\Library;

use Illuminate\Http\Request;
use App\Models\Library\Gratuity;
use App\Models\Library\SalesType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GratuityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $gratuities = Gratuity::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.gratuity.list', [
                'gratuities' => $gratuities
            ]);
        }
        return view('pages.backoffice.library.gratuity.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.gratuity.input');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'unique:gratuity,name'],
            'amount' => ['required', 'numeric', 'min:1', 'max:100'],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount')
                ]);
            }
        }
        Gratuity::create([
            'name' => $request->name,
            'amount' => $request->amount,
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Gratuity created.'
        ]);
    }
    public function show(Gratuity $gratuity)
    {
        //
    }
    public function edit(Gratuity $gratuity)
    {
        return view('pages.backoffice.library.gratuity.input', [
            'gratuity' => $gratuity
        ]);
    }
    public function update(Request $request, Gratuity $gratuity)
    {
        $rules = [
            'name' => ['required'],
            'amount' => ['required', 'numeric', 'min:1', 'max:100'],
        ];
        if ($request->name != $gratuity->name) {
            $rules['name'] += [count($rules['name']) => 'unique:gratuity,name'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount')
                ]);
            }
        }
        Gratuity::where('id', $gratuity->id)->update([
            'name' => $request->name,
            'amount' => $request->amount
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Gratuity updated.'
        ]);
    }
    public function destroy(Gratuity $gratuity)
    {
        foreach (SalesType::whereJsonContains('gratuity_applied', $gratuity->id)->get() as $salesType) {
            $salesType->gratuity_applied->forget($salesType->gratuity_applied->search($gratuity->id));
            $salesType->save();
        }
        $gratuity->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Gratuity deleted.'
        ]);
    }
}
