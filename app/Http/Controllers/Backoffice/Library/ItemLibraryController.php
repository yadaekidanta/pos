<?php

namespace App\Http\Controllers\Backoffice\Library;

use App\Http\Controllers\Controller;
use App\Models\Library\Brand;
use App\Models\Library\Item;
use App\Models\Library\ItemCategory;
use Illuminate\Http\Request;

class ItemLibraryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return view('pages.backoffice.library.item.list');
        }
        return view('pages.backoffice.library.item.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.item.input', [
            'categories' => ItemCategory::orderBy('name', 'asc')->get(),
            'brands' => Brand::orderBy('name', 'asc')->get()
        ]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Item $item)
    {
        //
    }
    public function edit(Item $item)
    {
        return view('pages.backoffice.library.item.input', [
            'categories' => ItemCategory::orderBy('name', 'asc')->get(),
        ]);
    }
    public function update(Request $request, Item $item)
    {
        //
    }
    public function destroy(Item $item)
    {
        //
    }
    public function export()
    {
        //
    }
}
