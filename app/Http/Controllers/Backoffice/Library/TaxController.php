<?php

namespace App\Http\Controllers\Backoffice\Library;

use App\Models\Library\Tax;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TaxController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $taxes = Tax::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.taxes.list', [
                'taxes' => $taxes
            ]);
        }
        return view('pages.backoffice.library.taxes.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.taxes.input');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'unique:taxes,name'],
            'amount' => ['required', 'numeric', 'min:1', 'max:100'],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount')
                ]);
            }
        }
        Tax::create([
            'name' => $request->name,
            'amount' => $request->amount,
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Tax created.'
        ]);
    }
    public function show(Tax $tax)
    {
        //
    }
    public function edit(Tax $tax)
    {
        return view('pages.backoffice.library.taxes.input', [
            'tax' => $tax
        ]);
    }
    public function update(Request $request, Tax $tax)
    {
        $rules = [
            'name' => ['required'],
            'amount' => ['required', 'numeric', 'min:1', 'max:100'],
        ];
        if ($request->name != $tax->name) {
            $rules['name'] += [count($rules['name']) => 'unique:taxes,name'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount')
                ]);
            }
        }
        Tax::where('id', $tax->id)->update([
            'name' => $request->name,
            'amount' => $request->amount
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Tax updated.'
        ]);
    }
    public function destroy(Tax $tax)
    {
        $tax->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tax deleted.'
        ]);
    }
}
