<?php

namespace App\Http\Controllers\Backoffice\Library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Library\ItemCategory;
use Illuminate\Support\Facades\Validator;

class ItemCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $categories = ItemCategory::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.categories.list', [
                'categories' => $categories,
            ]);
        }
        return view('pages.backoffice.library.categories.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.categories.input');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'unique:item_categories,name'],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
        }
        ItemCategory::create([
            'name' => $request->name,
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Category created.',
        ]);
    }
    public function show(ItemCategory $category)
    {
        //
    }
    public function edit(ItemCategory $category)
    {
        return view('pages.backoffice.library.categories.input', [
            'category' => $category,
        ]);
    }
    public function update(Request $request, ItemCategory $category)
    {
        $rules = [
            'name' => ['required']
        ];
        if ($request->name != $category->name) {
            $rules['name'] += [count($rules['name']) => 'unique:item_categories,name'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        ItemCategory::where('id', $category->id)->update([
            'name' => $request->name
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Category updated.',
        ]);
    }
    public function destroy(ItemCategory $category)
    {
        $category->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Category deleted.',
        ]);
    }
    public function export()
    {
        //
    }
    public function assign(Request $request, ItemCategory $categoy)
    {
        //
    }
}
