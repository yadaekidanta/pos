<?php

namespace App\Http\Controllers\Backoffice\Library;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Library\Discount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DiscountController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $discounts = Discount::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.discounts.list', [
                'discounts' => $discounts
            ]);
        }
        return view('pages.backoffice.library.discounts.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.discounts.input');
    }
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'unique:discounts,name'],
            'amount' => ['required', 'numeric'],
            'type' => [Rule::in(['%'])]
        ];
        if ($request->type ?? false) {
            $rules['amount'] += [count($rules['amount']) => 'min:1'];
            $rules['amount'] += [count($rules['amount']) => 'max:100'];
        } else {
            $rules['amount'] += [count($rules['amount']) => 'min:1'];
            $rules['amount'] += [count($rules['amount']) => 'max:99999999'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount')
                ]);
            }
            if ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type')
                ]);
            }
        }
        Discount::create([
            'name' => $request->name,
            'amount' => $request->amount,
            'type' => $request->type ?? false ? $request->type : 'Rp',
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Discount created.'
        ]);
    }
    public function show(Discount $discount)
    {
        //
    }
    public function edit(Discount $discount)
    {
        return view('pages.backoffice.library.discounts.input', [
            'discount' => $discount,
        ]);
    }
    public function update(Request $request, Discount $discount)
    {
        $rules = [
            'name' => ['required'],
            'amount' => ['required', 'numeric'],
            'type' => [Rule::in(['%'])]
        ];
        if ($request->name != $discount->name) {
            $rules['name'] += [count($rules['name']) => 'unique:discounts,name'];
        }
        if ($request->type ?? false) {
            $rules['amount'] += [count($rules['amount']) => 'min:1'];
            $rules['amount'] += [count($rules['amount']) => 'max:100'];
        } else {
            $rules['amount'] += [count($rules['amount']) => 'min:1'];
            $rules['amount'] += [count($rules['amount']) => 'max:99999999'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
            if ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount')
                ]);
            }
            if ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type')
                ]);
            }
        }
        Discount::where('id', $discount->id)->update([
            'name' => $request->name,
            'amount' => $request->amount,
            'type' => $request->type ?? false ? $request->type : 'Rp',
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Discount updated.'
        ]);
    }
    public function destroy(Discount $discount)
    {
        $discount->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Discount deleted.'
        ]);
    }
}
