<?php

namespace App\Http\Controllers\Backoffice\Library;

use Illuminate\Http\Request;
use App\Models\Library\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $brands = Brand::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.brands.list', [
                'brands' => $brands
            ]);
        }
        return view('pages.backoffice.library.brands.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.brands.input');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'unique:brands,name']
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
        }
        Brand::create([
            'name' => $request->name
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Brand created.'
        ]);
    }
    public function show(Brand $brand)
    {
        //
    }
    public function edit(Brand $brand)
    {
        return view('pages.backoffice.library.brands.input', [
            'brand' => $brand
        ]);
    }
    public function update(Request $request, Brand $brand)
    {
        $rules = [
            'name' => ['required']
        ];
        if ($request->name != $brand->name) {
            $rules['name'] += [count($rules['name']) => 'unique:brands,name'];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name')
                ]);
            }
        }
        Brand::where('id', $brand->id)->update([
            'name' => $request->name,
        ]);
        return response()->json([
            'alert' => 'success',
            'message' => 'Brand updated.'
        ]);
    }
    public function destroy(Brand $brand)
    {
        $brand->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Brand deleted.'
        ]);
    }
}
