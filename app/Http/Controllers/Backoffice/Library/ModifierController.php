<?php

namespace App\Http\Controllers\Backoffice\Library;

use App\Http\Controllers\Controller;
use App\Models\Library\Modifier;
use Illuminate\Http\Request;

class ModifierController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $collection = Modifier::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'desc')->paginate(10);
            return view('pages.backoffice.library.modifier.list', [
                'collection' => $collection
            ]);
        }
        return view('pages.backoffice.library.modifier.main');
    }
    public function create()
    {
        return view('pages.backoffice.library.modifier.input');
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Modifier $modifier)
    {
        return view('pages.backoffice.library.modifier.show',['data' => $modifier]);
    }
    public function edit(Modifier $modifier)
    {
        return view('pages.backoffice.library.modifier.input',['data' => $modifier]);
    }
    public function update(Request $request, Modifier $modifier)
    {
        //
    }
    public function destroy(Modifier $modifier)
    {
        //
    }
}
