<?php

namespace App\Http\Controllers\Backoffice\Library;

use App\Http\Controllers\Controller;
use App\Models\Library\Promo;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Promo $promo)
    {
        //
    }
    public function edit(Promo $promo)
    {
        //
    }
    public function update(Request $request, Promo $promo)
    {
        //
    }
    public function destroy(Promo $promo)
    {
        //
    }
}
