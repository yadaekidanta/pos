<?php

namespace App\Http\Controllers\Backoffice\Ingredient;

use App\Http\Controllers\Controller;
use App\Models\Ingredient\Recipe;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Recipe $recipe)
    {
        //
    }
    public function edit(Recipe $recipe)
    {
        //
    }
    public function update(Request $request, Recipe $recipe)
    {
        //
    }
    public function destroy(Recipe $recipe)
    {
        //
    }
}
