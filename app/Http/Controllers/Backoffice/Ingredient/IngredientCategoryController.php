<?php

namespace App\Http\Controllers\Backoffice\Ingredient;

use App\Http\Controllers\Controller;
use App\Models\Ingredient\IngredientCategory;
use Illuminate\Http\Request;

class IngredientCategoryController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(IngredientCategory $ingredientCategory)
    {
        //
    }
    public function edit(IngredientCategory $ingredientCategory)
    {
        //
    }
    public function update(Request $request, IngredientCategory $ingredientCategory)
    {
        //
    }
    public function destroy(IngredientCategory $ingredientCategory)
    {
        //
    }
}
