<?php

namespace App\Http\Controllers\Backoffice\Ingredient;

use App\Http\Controllers\Controller;
use App\Models\Ingredient\Ingredient;
use Illuminate\Http\Request;

class IngredientLibraryController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Ingredient $ingredient)
    {
        //
    }
    public function edit(Ingredient $ingredient)
    {
        //
    }
    public function update(Request $request, Ingredient $ingredient)
    {
        //
    }
    public function destroy(Ingredient $ingredient)
    {
        //
    }
}
