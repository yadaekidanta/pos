<?php

namespace App\Http\Controllers\Backoffice\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\PurchaseOrder;
use Illuminate\Http\Request;

class PurchaseOrderController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(PurchaseOrder $purchaseOrder)
    {
        //
    }
    public function edit(PurchaseOrder $purchaseOrder)
    {
        //
    }
    public function update(Request $request, PurchaseOrder $purchaseOrder)
    {
        //
    }
    public function destroy(PurchaseOrder $purchaseOrder)
    {
        //
    }
}
