<?php

namespace App\Http\Controllers\Backoffice\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Adjustment;
use Illuminate\Http\Request;

class AdjustmentController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Adjustment $adjustment)
    {
        //
    }
    public function edit(Adjustment $adjustment)
    {
        //
    }
    public function update(Request $request, Adjustment $adjustment)
    {
        //
    }
    public function destroy(Adjustment $adjustment)
    {
        //
    }
}
