<?php

namespace App\Http\Controllers\Backoffice\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Transfer;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Transfer $transfer)
    {
        //
    }
    public function edit(Transfer $transfer)
    {
        //
    }
    public function update(Request $request, Transfer $transfer)
    {
        //
    }
    public function destroy(Transfer $transfer)
    {
        //
    }
}
