<?php

namespace App\Http\Controllers\Backoffice\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Supplier $supplier)
    {
        //
    }
    public function edit(Supplier $supplier)
    {
        //
    }
    public function update(Request $request, Supplier $supplier)
    {
        //
    }
    public function destroy(Supplier $supplier)
    {
        //
    }
}
