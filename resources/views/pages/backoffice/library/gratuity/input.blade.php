<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <h3>
            {{ $gratuity?? false ? 'Edit' : 'Create' }} Gratuity
        </h3>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Gratuity Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                        value="{{ $gratuity ?? false ? $gratuity->name : '' }}" />
                </div>
                <div class="fv-row mb-7 ">
                    <label class="required fs-6 fw-bold mb-2">Amount</label>
                    <div class="input-group input-group-solid">
                        <input type="text" class="form-control form-control-solid" placeholder="" name="amount"
                            value="{{ $gratuity ?? false ? $gratuity->amount : '' }}"
                            oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"
                            maxlength="3" aria-describedby="basic-addon2" />
                        <span class="input-group-text" id="basic-addon2">%</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($gratuity?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.library.gratuity.destroy',$gratuity->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $gratuity ?? false ? route('backoffice.library.gratuity.update', $gratuity->id) : route('backoffice.library.gratuity.store')}}', '{{ $gratuity ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>