<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <h3>
            {{ $brand ?? false ? 'Edit' : 'Create' }} Brand
        </h3>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Brand Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                        value="{{ $brand ?? false ? $brand->name : '' }}" />
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($brand ?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.library.brands.destroy',$brand->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $brand ?? false ? route('backoffice.library.brands.update', $brand->id) : route('backoffice.library.brands.store')}}', '{{ $brand ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>