<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <h3>
            {{ $data ?? false ? 'Edit' : 'Create' }} Modifier
        </h3>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Modifier Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                        value="{{ $data ?? false ? $data->name : '' }}" />
                </div>
                <div id="form_repeater_modifier">
                    <!--begin::Form group-->
                    <div class="form-group">
                        <div data-repeater-list="form_repeater_modifier">
                            <div data-repeater-item>
                                <div class="form-group row">
                                    <div class="col-md-5 mt-3">
                                        <label class="form-label">Option Name:</label>
                                        <input type="email" class="form-control mb-2 mb-md-0" placeholder="Option Name" />
                                    </div>
                                    <div class="col-md-5 mt-3">
                                        <label class="form-label">Option Price:</label>
                                        <input type="email" class="form-control mb-2 mb-md-0" placeholder="Rp. 0" />
                                    </div>
                                    <div class="col-md-2 mt-3">
                                        <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                            <i class="la la-trash-o"></i>Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Form group-->
                
                    <!--begin::Form group-->
                    <div class="form-group mt-5">
                        <a href="javascript:;" data-repeater-create class="btn btn-block btn-light-primary">
                            <i class="la la-plus"></i>Add
                        </a>
                    </div>
                    <!--end::Form group-->
                </div>
                <div class="d-flex flex-stack w-lg-50 mt-5">
                    <!--begin::Label-->
                    <div class="me-5">
                        <label class="fs-6 fw-bold form-label">Modifier Configuration</label>
                        {{-- <div class="fs-7 fw-bold text-muted">If you need more info, please check budget planning</div> --}}
                    </div>
                    <!--end::Label-->
                
                    <!--begin::Switch-->
                    <label class="form-check form-switch form-check-custom form-check-solid float-end">
                        <input class="form-check-input" id="configuration" name="configuration" type="checkbox" value="1"/>
                    </label>
                    <!--end::Switch-->
                </div>
                <div id="configuration_content" class="mt-5">
                    <div data-kt-buttons="true">
                        <!--begin::Radio button-->
                        <label class="btn btn-outline btn-outline-dashed d-flex flex-stack text-start p-6 mb-5 active">
                            <!--end::Description-->
                            <div class="d-flex align-items-center me-2">
                                <!--begin::Radio-->
                                <div class="form-check form-check-custom form-check-solid form-check-primary me-6">
                                    <input class="form-check-input" onclick="required_content('n');" type="radio" name="required" checked="checked" value="n"/>
                                </div>
                                <!--end::Radio-->
                    
                                <!--begin::Info-->
                                <div class="flex-grow-1">
                                    <h2 class="d-flex align-items-center fs-3 fw-bolder flex-wrap">
                                        No
                                    </h2>
                                    <div class="fw-bold opacity-50">
                                        Modifier is optional
                                    </div>
                                </div>
                                <!--end::Info-->
                            </div>
                            <!--end::Description-->
                    
                            <!--begin::Price-->
                            <div class="ms-5 d-none">
                                <span class="mb-2">$</span>
                                <span class="fs-2x fw-bolder">
                                    39
                                </span>
                                <span class="fs-7 opacity-50">/
                                    <span data-kt-element="period">Mon</span>
                                </span>
                            </div>
                            <!--end::Price-->
                        </label>
                        <!--end::Radio button-->
                    
                        <!--begin::Radio button-->
                        <label class="btn btn-outline btn-outline-dashed d-flex flex-stack text-start p-6 mb-5">
                            <!--end::Description-->
                            <div class="d-flex align-items-center me-2">
                                <!--begin::Radio-->
                                <div class="form-check form-check-custom form-check-solid form-check-primary me-6">
                                    <input class="form-check-input" onclick="required_content('y');" type="radio" name="required" value="y"/>
                                </div>
                                <!--end::Radio-->
                    
                                <!--begin::Info-->
                                <div class="flex-grow-1">
                                    <h2 class="d-flex align-items-center fs-3 fw-bolder flex-wrap">
                                        Yes
                                        {{-- <span class="badge badge-light-success ms-2 fs-7">Most popular</span> --}}
                                    </h2>
                                    <div class="fw-bold opacity-50">
                                        Modifier selection is required
                                    </div>
                                </div>
                                <!--end::Info-->
                            </div>
                            <!--end::Description-->
                    
                            <!--begin::Price-->
                            <div class="ms-5 d-none">
                                <span class="mb-2">$</span>
                                <span class="fs-2x fw-bolder">
                                    139
                                </span>
                                <span class="fs-7 opacity-50">/
                                    <span data-kt-element="period">Mon</span>
                                </span>
                            </div>
                            <!--end::Price-->
                        </label>
                        <!--end::Radio button-->
                    </div>
                    <div id="required_content" class="mt-5">
                        <div class="fv-row mb-7">
                            <label class="required fs-6 fw-bold mb-2">Min. number of modifier selected</label>
                            <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                                value="{{ $data ?? false ? $data->name : '' }}" />
                        </div>
                    </div>
                    <div class="fv-row">
                        <label class="required fs-6 fw-bold mb-2">Max. number of modifier selected</label>
                        <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                            value="{{ $data ?? false ? $data->name : '' }}" />
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($data ?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.library.modifier.destroy',$data->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $data ?? false ? route('backoffice.library.modifier.update', $data->id) : route('backoffice.library.modifier.store')}}', '{{ $data ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>
<script type="text/javascript">
    $("#configuration_content").hide();
    $("#required_content").hide();
    form_repeater('form_repeater_modifier');
    $('#configuration').on('change', function() {
        if($("#configuration").is(':checked')) {
            $("#configuration_content").show();
        }else{
            $("#configuration_content").hide();
        }
    });
    function required_content(val){
        if(val == "n"){
            $("#required_content").hide();
        }else{
            $("#required_content").show();
        }
    }
</script>