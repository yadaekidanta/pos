<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <h3>
            {{ $salesType ?? false ? 'Edit' : 'Create' }} Item
        </h3>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row">
                    <label class="fs-6 fw-bold mb-2">Image for POS</label>
                </div>
                <div class="fv-row mb-7">
                    <img id="img-preview" class="img-thumbnail img-fluid mb-3 w-125px"
                        src="{{ asset('img/no-image.png') }}">
                    <input type="file" class="form-control form-control-solid" id="image" name="image_pos"
                        onchange="previewImage()">
                </div>
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Item Name</label>
                    <input type="text" class="form-control form-control-solid" name="name"
                        value="{{ $item->name ?? false ? $item->name : '' }}" />
                </div>
                <div class="fv-row mb-7">
                    <label class="fs-6 fw-bold mb-2">Category</label>
                    <select class="form-select form-select-solid" name="category_id">
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="fv-row mb-7">
                    <label class="fs-6 fw-bold mb-2">Description</label>
                    <textarea name="description" id="description" class="form-control form-control-solid"></textarea>
                </div>
                <div class="fv-row mb-7">
                    <div class="d-flex flex-stack">
                        <div class="me-5">
                            <label class="fs-6 fw-bold">Online Attributes</label>
                        </div>
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input class="form-check-input" name="is_ecommerce" type="checkbox" value="yes"
                                id="is_ecommerce" />
                        </label>
                    </div>
                </div>
                <div class="separator mb-7"></div>
                <div id="is_ecommerce_div" class="d-none">
                    <div class="fv-row mb-7">
                        <label class="fs-6 fw-bold mb-2 required">Brand Name</label>
                        <select class="form-select form-select-solid" name="brand_id">
                            @foreach ($brands as $brand)
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="fv-row mb-7">
                        <label class="fs-6 fw-bold mb-2 required">Condition</label>
                        <select class="form-select form-select-solid" id="condition" name="condition">
                            <option value="New">New</option>
                            <option value="Refurbished">Refurbished</option>
                            <option value="Used">Used</option>
                        </select>
                    </div>
                    <div class="fv-row mb-7">
                        <label class="required fs-6 fw-bold mb-2">Weight</label>
                        <div class="row gy-2 gx-3 align-items-center">
                            <div class="col-auto">
                                <input type="text" class="form-control form-control-solid" name="weight"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"
                                    maxlength="8" />
                            </div>
                            <div class="col-auto">
                                <select class="form-select form-select-solid" name="weight_type">
                                    <option value="kg">Kilogram (Kg)</option>
                                    <option value="gr">Gram (Gr)</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="fv-row mb-7">
                        <label class="required fs-6 fw-bold mb-2">Dimension</label>
                        <div class="row gy-2 gx-3 align-items-center">
                            <div class="col">
                                <input type="text" class="form-control form-control-solid" name="length"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"
                                    maxlength="8" placeholder="Length" />
                            </div>
                            <div class="col">
                                <input type="text" class="form-control form-control-solid" name="width"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"
                                    maxlength="8" placeholder="Width" />
                            </div>
                            <div class="col">
                                <input type="text" class="form-control form-control-solid" name="height"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"
                                    maxlength="8" placeholder="Height" />
                            </div>
                            <div class="col">
                                <select class="form-select form-select-solid" name="dimension_type">
                                    <option value="cm">Centimeter (Cm)</option>
                                    <option value="m">Meter (M)</option>
                                    <option value="in">Inch (In)</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="fv-row mb-7">
                        <label class="required fs-6 fw-bold mb-2">Product Image</label>
                        <div class="dropzone" id="product_image">
                            <!--begin::Message-->
                            <div class="dz-message needsclick">
                                <!--begin::Icon-->
                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                <!--end::Icon-->
                
                                <!--begin::Info-->
                                <div class="ms-4">
                                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                                    <span class="fs-7 fw-bold text-gray-400">Upload up to 10 files</span>
                                </div>
                                <!--end::Info-->
                            </div>
                        </div>
                    </div>
                    <div id="form_repeater_pricing">
                        <!--begin::Form group-->
                        <div class="form-group">
                            <div data-repeater-list="form_repeater_pricing">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-md-3 mt-3">
                                            <label class="form-label">Varian Name:</label>
                                            <input type="email" class="form-control mb-2 mb-md-0" placeholder="" />
                                        </div>
                                        <div class="col-md-3 mt-3">
                                            <label class="form-label">Varian Price:</label>
                                            <input type="email" class="form-control mb-2 mb-md-0" placeholder="Rp. 0" />
                                        </div>
                                        <div class="col-md-3 mt-3">
                                            <label class="form-label">Varian SKU:</label>
                                            <input type="email" class="form-control mb-2 mb-md-0" placeholder="" />
                                        </div>
                                        <div class="col-md-3 mt-3">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                                <i class="la la-trash-o"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Form group-->
                    
                        <!--begin::Form group-->
                        <div class="form-group mt-5">
                            <a href="javascript:;" data-repeater-create class="btn btn-block btn-light-primary">
                                <i class="la la-plus"></i>Add
                            </a>
                        </div>
                        <!--end::Form group-->
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($salesType ?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.library.sales-type.destroy',$salesType->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $salesType ?? false ? route('backoffice.library.sales-type.update', $salesType->id) : route('backoffice.library.sales-type.store')}}', '{{ $salesType ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>

<script>
    document.getElementById("is_ecommerce").addEventListener("change", function (event) {
    if (event.target.checked) {
    document.getElementById('is_ecommerce_div').classList.remove("d-none");
    } else {
    document.getElementById('is_ecommerce_div').classList.add("d-none");
    }
    });
    obj_select('condition');
    obj_dropzone("product_image","{{route('backoffice.auth.index')}}","12","8000");
    form_repeater('form_repeater_pricing');
</script>