<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <h3>
            {{ $discount ?? false ? 'Edit' : 'Create' }} Discount
        </h3>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Discount Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                        value="{{ $discount ?? false ? $discount->name : '' }}" />
                </div>
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Amount</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="amount"
                        value="{{ $discount ?? false ? $discount->amount : '' }}"
                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"
                        maxlength="8" />
                </div>
                <div class="fv-row mb-7">
                    <div class="d-flex flex-stack">
                        <div class="me-5">
                            <label class="fs-6 fw-bold">Percent or Rupiah?</label>
                            <div class="fs-7 fw-bold text-muted">Check to make amount type Percent</div>
                        </div>
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <span class="form-check-label fw-bold text-muted me-3">Rp</span>
                            @if ($discount ?? false)
                            <input class="form-check-input" name="type" type="checkbox" value="%" {{ $discount->type
                            == '%' ?
                            "checked='checked'" : '' }} />
                            @else
                            <input class="form-check-input" name="type" type="checkbox" value="%" />
                            @endif
                            <span class="form-check-label fw-bold text-muted">%</span>
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($discount ?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.library.discounts.destroy',$discount->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $discount ?? false ? route('backoffice.library.discounts.update', $discount->id) : route('backoffice.library.discounts.store')}}', '{{ $discount ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>