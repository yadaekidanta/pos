<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <h3>
            {{ $tableGroup ?? false ? 'Edit' : 'Create' }} Table Group
        </h3>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Table Group Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                        value="{{ $tableGroup->id ?? false ? $tableGroup->name : '' }}" />
                </div>
                <div class="fv-row mb-7">
                    <div class="d-flex flex-stack">
                        <div class="me-5">
                            <label class="fs-6 fw-bold">Status Active?</label>
                            <div class="fs-7 fw-bold text-muted">Check to activate table group</div>
                        </div>
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            @if ($tableGroup->id ?? false)
                            <input class="form-check-input" name="status" type="checkbox" value="active" {{
                                $tableGroup->status == 'active' ? "checked='checked'" : '' }}
                            />
                            @else
                            <input class="form-check-input" name="status" type="checkbox" value="active"
                                checked="checked" />
                            @endif
                            <span class="form-check-label fw-bold text-muted">Yes</span>
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($tableGroup ?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.table-management.table-group.destroy',$tableGroup->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $tableGroup ?? false ? route('backoffice.table-management.table-group.update', $tableGroup->id) : route('backoffice.table-management.table-group.store')}}', '{{ $tableGroup ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>