<table class="table align-middle table-row-dashed fs-6 gy-5" id="table">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Table Group</th>
            <th class="min-w-125px">Status</th>
            <th class="min-w-125px">Table Count</th>
            <th class="text-end min-w-70px"></th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($tableGroups as $tableGroup)
        <tr>
            <td>{{ $tableGroup->name }}</td>
            <td class="{{ $tableGroup->status == 'active' ? 'text-success' : 'text-danger' }}">
                {{ $tableGroup->status }}
            </td>
            <td>{{ $tableGroup->tableMap->count() }}</td>
            <td class="text-end">
                <div class="btn-group" role="group">
                    <button id="action" type="button" class="btn btn-sm btn-light btn-active-light-primary"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Action
                        <span class="svg-icon svg-icon-5 m-0">
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path
                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                        fill="currentColor" />
                                </svg>
                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                        aria-labelledby="action">
                        <div class="menu-item px-3">
                            <a href="javascript:;" class="menu-link px-3"
                                onclick="load_input('{{route('backoffice.table-management.table-group.edit',$tableGroup->id)}}');">Edit</a>
                        </div>
                        <div class="menu-item px-3">
                            <a href="javascript:;" class="menu-link px-3"
                                onclick="handle_confirm('Confirm to duplicate?','Yes','No','POST','{{route('backoffice.table-management.table-group.duplicate',$tableGroup->id)}}');">Duplicate</a>
                        </div>
                        <div class="menu-item px-3">
                            <a href="javascript:;" class="menu-link px-3"
                                onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.table-management.table-group.destroy',$tableGroup->id)}}');">Delete</a>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$tableGroups->links('themes.backoffice.pagination')}}