<div class="card">
    <div class="card-header border-0 pt-6" id="input_header">
        <div class="card-title">
            <h3>
                {{ $tableMap ?? false ? 'Edit' : 'Add' }} Table
            </h3>
        </div>
    </div>
    <div class="card-body pt-0">
        <form id="form_input">
            <div class="scroll-y me-n7 pe-7" id="konten_input" data-kt-scroll="true"
                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                data-kt-scroll-dependencies="#input_header" data-kt-scroll-wrappers="#konten_input"
                data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Table Group</label>
                    <select name="table_group_id" class="form-select form-select-solid fw-bolder">
                        @foreach ($tableGroups as $tableGroup)
                        @if ($tableMap ?? false)
                        @if ($tableMap->table_group_id == $tableGroup->id)
                        <option value="{{ $tableGroup->id }}" selected>{{ $tableGroup->name }}</option>
                        @endif
                        @else
                        <option value="{{ $tableGroup->id }}">{{ $tableGroup->name }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Table Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="" name="name"
                        value="{{ $tableMap ?? false ? $tableMap->name : '' }}" />
                </div>
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Pax</label>
                    <input type="number" class="form-control form-control-solid" min="1" max="999" name="pax"
                        value="{{ $tableMap ?? false ? $tableMap->pax : '1' }}" />
                </div>
                <div class="fv-row mb-7">
                    <label class="required fs-6 fw-bold mb-2">Shape</label>
                </div>
                <div class="fv-row mb-7">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="shape" id="circle" value="circle" {{
                            $tableMap ?? false ? 'checked' : '' }}>
                        <label class="form-check-label" for="circle">
                            Circle
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="shape" id="square" value="square" {{
                            $tableMap ?? false ? 'checked' : '' }}>
                        <label class="form-check-label" for="square">
                            Square
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        @if ($tableMap ?? false)
        <button type="button" class="btn btn-danger float-left"
            onclick="handle_confirm('Are you sure?','Yes','No','DELETE','{{route('backoffice.table-management.table-map.destroy',$tableMap->id)}}');">Delete</button>
        @endif
        <button id="btn-save" type="button" class="btn btn-primary" style="float:right;"
            onclick="handle_save('#btn-save', '#form_input', '{{ $tableMap ?? false ? route('backoffice.table-management.table-map.update', $tableMap->id) : route('backoffice.table-management.table-map.store')}}', '{{ $tableMap ?? false ? 'PATCH' : 'POST' }}');">
            Save
        </button>
        <button type="button" class="btn btn-dark me-5" style="float:right;" onclick="load_list(1);">Back</button>
    </div>
</div>