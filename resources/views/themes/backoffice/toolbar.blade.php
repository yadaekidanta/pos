<div class="toolbar bg_gradasi" id="kt_toolbar">
    <!--begin::Container-->
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        @yield('toolbar_title')
        <!--end::Page title-->
        <!--begin::Actions-->
        @yield('toolbar_action')
        <!--end::Actions-->
    </div>
    <!--end::Container-->
</div>