<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->integer('outlet_id');
            $table->integer('quantity');
            $table->longtext('note');
            $table->string('item');
            $table->timestamps();
        });
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longtext('address');
            $table->string('phone',15)->unique();
            $table->string('email',500)->unique();
            $table->string('city');
            $table->string('state');
            $table->integer('postcode');
            $table->timestamps();
        });
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('outlet_id');
            $table->unsignedBigInteger('supplier_id');
            $table->integer('order_no');
            $table->integer('created_by');
            $table->longtext('note');
            $table->float('total',20,0,true);
            $table->string('st');
            $table->timestamps();
        });
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('purchase_order_id');
            $table->json('product');
            /*
                BISA MULTI ITEM
                [
                    "Name": "",
                    "Qty": "",
                    "Subtotal": "",
                    BISA MULTI VARIAN / SELECT * VARIAN PER ITEMNYA
                    "varian": [
                        "Varian Name": ""
                    ]
                ]
            */
            $table->timestamps();
        });
        Schema::create('adjustments', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('outlet_id');
            $table->string('item');
            $table->string('variant');
            $table->integer('sku');
            $table->integer('in_stock');
            $table->integer('actual_stock');
            $table->integer('adjustment');
            $table->longtext('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
        Schema::dropIfExists('suppliers');
        Schema::dropIfExists('purchase_orders');
        Schema::dropIfExists('adjustments');
    }
}
