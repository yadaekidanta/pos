<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name',500);
            $table->string('email',500)->unique();
            $table->string('phone',20)->unique();
            $table->enum('sex',['male','female','etc'])->default('etc');
            $table->longtext('address');
            $table->integer('city_id');
            $table->integer('country_id');
            $table->integer('postcode');
            $table->datetime('customer_since');
            $table->datetime('last_visit');
            $table->string('total_of_orders');
            $table->string('amount_this_month');
            $table->string('amount_this_year');
            $table->string('amount_lifetime');
            $table->string('amount_average');
            $table->datetime('member_since');
            $table->string('current_point_balance');
            $table->string('reward_redeemed');
            $table->string('member_spending');
            $table->timestamps();
        });
        /*
            Masih belum tau detail isi         
        */
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->longtext('feedback');
            $table->timestamps();
        });
        /*
            Masih belum tau detail isi         
        */
        Schema::create('loyalty_programs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
        Schema::dropIfExists('feedbacks');
        Schema::dropIfExists('loyalty_programs');
    }
}
