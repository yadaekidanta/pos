<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryModules extends Migration
{
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->integer('outlet_id')->default(0);
            $table->integer('category_id')->default(0);
            $table->string('sku', 10)->nullable();
            $table->string('name');
            $table->string('thumbnail')->nullable();
            $table->enum('is_ecommerce', ['no', 'yes'])->default('no');
            // is ecommerce == y start
            $table->integer('brand_id')->default(0);
            $table->enum('is_preorder', ['n', 'y'])->default('n');
            $table->float('process_day', 2)->default(0);
            $table->float('weight', 3)->default(0);
            $table->enum('weight_type', ['kg', 'gr'])->default('kg');
            $table->float('length', 3)->default(0);
            $table->float('width', 3)->default(0);
            $table->float('height', 3)->default(0);
            $table->enum('dimension_type', ['cm', 'm', 'in'])->default('cm');
            $table->enum('condition', ['New', 'Refurbished', 'Used'])->nullable();
            // is ecommerce == y end
            $table->enum('is_sales_type', ['n', 'y'])->default('n');
            $table->json('variant')->nullable();
            /*
                variant format if is_sales_type is n
                BISA MULTI ITEM
                [
                    "Varian Name": "",
                    "Base Price": "",
                    "SKU": ""
                ]
                variant format if is_sales_type is y
                BISA MULTI ITEM
                [
                    "Varian Name": "",
                    "SKU": ""
                ]
            */
            $table->json('sales_type')->nullable();
            /*
                pricing_sales_type format if is_sales_type is n
                [
                ]
                pricing_sales_type format if is_sales_type is y
                SALES TYPE YG SELECTED DITAMPILKAN
                BISA MULTI ITEM
                [
                    "Sales Type Name": "",
                    "Price": "",
                    "SKU": ""
                ]
            */
            $table->json('inventory')->nullable();
            /*
                BISA MULTI ITEM
                [
                    "Varian Name": "",
                    "Track Stok": "", CHECKBOX, IF CHECKED ? Y : N
                    "In Stock": "",
                    "Alert": "",  CHECKBOX, IF CHECKED ? Y : N
                    "Alert At": ""
                ]
            */
            $table->json('cogs')->nullable();
            /*
                BISA MULTI ITEM
                [
                    "Varian Name": "",
                    "Track COGS": "", CHECKBOX, IF CHECKED ? Y : N
                    "Cost": "",
                ]
            */
            $table->timestamps();
        });
        Schema::create('item_images', function (Blueprint $table) {
            $table->id();
            $table->integer('item_id')->default(0);
            $table->string('file');
            $table->timestamps();
        });
        Schema::create('modifiers', function (Blueprint $table) {
            $table->id();
            $table->json('item_id')->nullable();
            $table->string('name');
            $table->json('option')->nullable();
            $table->enum('is_required', ['n', 'y'])->default('n');
            $table->enum('modifier_configuration', ['n', 'y'])->default('n');
            $table->float('min_select', 3)->default(0);
            $table->float('max_select', 3)->default(0);
            $table->timestamps();
        });
        Schema::create('item_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('promos', function (Blueprint $table) {
            $table->id();
            $table->enum('promo_type', ['Discount Per Item', 'Free Item'])->default('Discount Per Item');
            $table->string('name');
            $table->json('outlet_id')->nullable();
            $table->enum('sales_type', ['All Sales Type', 'Specific Sales Type'])->default('All Sales Type');
            $table->json('sales_type_id')->nullable();
            $table->enum('is_per_item', ['n', 'y'])->default('n');
            $table->json('item')->nullable();
            /*
                JIKA is_per_item = y
                BISA MULTI ITEM
                [
                    "Qty": "",
                    "Item": "",
                    BISA MULTI VARIAN / SELECT * VARIAN PER ITEMNYA
                    "varian": [
                        "Varian Name": ""
                    ]
                ]
                JIKA is_per_item = n
                [
                    "Qty": "",
                    "Category": "",
                ]
            */
            $table->float('discount_amount', 10)->default(0);
            $table->enum('discount_type', ['%', 'Rp'])->default('%');
            $table->enum('is_multiple_item', ['n', 'y'])->default('n');
            // (e.g. if there is a '5% off', customer who buy 2 will get 5% off for 2 items, buy 3 will get 5% off for 3 items, etc.)
            $table->enum('is_set_promo', ['n', 'y'])->default('n');
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->string('hour_start', 5)->nullable();
            $table->string('hour_end', 5)->nullable();
            $table->json('days')->nullable();
            $table->timestamps();
        });
        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('amount', 10)->default(0);
            $table->enum('type', ['%', 'Rp'])->default('%');
            $table->timestamps();
        });
        Schema::create('taxes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('amount', 5)->default(0);
            $table->timestamps();
        });
        Schema::create('gratuity', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('amount', 5)->default(0);
            $table->timestamps();
        });
        Schema::create('sales_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->json('gratuity_applied')->nullable();
            $table->timestamps();
        });
        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('items');
        Schema::dropIfExists('item_images');
        Schema::dropIfExists('modifiers');
        Schema::dropIfExists('item_categories');
        Schema::dropIfExists('discounts');
        Schema::dropIfExists('taxes');
        Schema::dropIfExists('gratuity');
        Schema::dropIfExists('sales_type');
        Schema::dropIfExists('brands');
    }
}
