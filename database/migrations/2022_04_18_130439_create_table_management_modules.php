<?php

use App\Models\TableManagement\TableGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableManagementModules extends Migration
{
    public function up()
    {
        Schema::create('table_groups', function (Blueprint $table) {
            $table->id();
            $table->integer('outlet_id')->default(0);
            $table->string('name');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();
        });
        Schema::create('table_maps', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(TableGroup::class, 'table_group_id')->references('id')->on('table_groups')->cascadeOnDelete();
            $table->string('name');
            $table->enum('shape', ['circle', 'square'])->default('circle');
            $table->float('pax', 3);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('table_groups');
        Schema::dropIfExists('table_maps');
    }
}
