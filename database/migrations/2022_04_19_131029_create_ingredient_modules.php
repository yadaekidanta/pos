<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientModules extends Migration
{
    public function up()
    {
        Schema::create('ingredient_units', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('ingredient_items', function (Blueprint $table) {
            $table->id();
            $table->integer('outlet_id')->default(0);
            $table->enum('ingredient_type',['Semi Finished Ingredient','Raw Ingredient']);
            $table->integer('ingredient_unit_id')->default(0);
            $table->json('recipe')->nullable();
            /*
            if ingredient_type == Semi Finished Ingredient
                [
                    "Amount" : "",
                    [
                        "Ingredient Name" : "",
                        "Quantity" : "",
                        "Unit" : "",
                        "Avg Cost" : ""
                    ],
                    [
                        "Ingredient Name" : "",
                        "Quantity" : "",
                        "Unit" : "",
                        "Avg Cost" : ""
                    ],
                ]
            if ingredient_type == Raw Ingredient
                [
                    TIDAK ADA
                ]
            */
            $table->json('inventory')->nullable();
            /*
            if ingredient_type == Semi Finished Ingredient ? Raw Ingredient
                [
                    "Ingredient Name": "",
                    "Track Stok": "", CHECKBOX, IF CHECKED ? Y : N
                    "In Stock": "",
                    "Alert": "",  CHECKBOX, IF CHECKED ? Y : N
                    "Alert At": ""
                ]
            */
            $table->json('production')->nullable();
            /*
            JIKA PRODUCE, MAKA IN STOCK BERTAMBAH
            if ingredient_type == Semi Finished Ingredient
                [
                    "Ingredient Name": "",
                    "Produce": "",
                    "Unit": ""
                ]
            if ingredient_type == Raw Ingredient
                [
                    TIDAK ADA
                ]
            */
            $table->json('cogs')->nullable();
            /*
            if ingredient_type == Semi Finished Ingredient
                [
                    "Ingredient Name": "",
                    "Track COGS": "", CHECKBOX, IF CHECKED ? Y : N
                    "Cost": "", Total dari penjumlahan per raw recipes
                    "Unit": "", Unit yg Dipilih
                ]
            if ingredient_type == Raw Ingredient
                [
                    "Ingredient Name": "",
                    "Track COGS": "", CHECKBOX, IF CHECKED ? Y : N
                    "Cost": "",
                    "Unit": "", Unit yg Dipilih
                ]
            */
            $table->timestamps();
        });
        Schema::create('ingredient_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
            $table->integer('item_id')->default(0);
            $table->string('varian_name');
            $table->json('ingredient');
            /*
            BERDASARKAN PILIHAN
            [
                "Ingredient Name": "",
                "Quantity": "",
                "Unit": ""
                "Cost": ""
            ]
            */
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('ingredient_units');
        Schema::dropIfExists('ingredient_items');
        Schema::dropIfExists('ingredient_categories');
        Schema::dropIfExists('recipes');
    }
}
