<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone',20)->unique();
            $table->date('expiration_date');
            $table->integer('pin');
            $table->unsignedBigInteger('outlet_id');
            $table->enum('status',['active','inactive']);
            $table->longtext('description');
            $table->enum('role',['administrator','cashier']);
            $table->timestamps();
        });
        
        /*
            Masih belum tau detail isi         
        */
        Schema::create('employee_access', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
        /*
            Masih belum tau detail isi         
        */
        Schema::create('pin_access', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('employee_access');
        Schema::dropIfExists('pin_access');
    }
}
