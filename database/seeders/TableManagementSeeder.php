<?php

namespace Database\Seeders;

use App\Models\TableManagement\TableGroup;
use App\Models\TableManagement\TableMap;
use Illuminate\Database\Seeder;

class TableManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 3; $i++) {
            TableGroup::create([
                'outlet_id' => 1,
                'name' => 'Floor ' . $i + 1,
                'status' => rand() % 2 == 0 ?  'active' : 'inactive',
            ]);
        }
        for ($i = 0; $i < 50; $i++) {
            TableMap::create([
                'table_group_id' => rand(1, 3),
                'name' => 'Table ' . $i + 1,
                'shape' => rand() % 2 == 0 ?  'square' : 'circle',
                'pax' => rand(1, 8)
            ]);
        }
    }
}
