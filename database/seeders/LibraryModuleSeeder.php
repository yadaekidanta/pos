<?php

namespace Database\Seeders;

use App\Models\Library\Brand;
use Illuminate\Database\Seeder;
use App\Models\Library\Discount;
use App\Models\Library\Gratuity;
use App\Models\Library\ItemCategory;
use App\Models\Library\SalesType;
use App\Models\Library\Tax;

class LibraryModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Application'],
            ['name' => 'Hardware'],
            ['name' => 'Software'],
            ['name' => 'Operating System'],
            ['name' => 'Database'],
            ['name' => 'Data Sheet'],
            ['name' => 'Game'],
            ['name' => 'Video'],
            ['name' => 'Image'],
            ['name' => 'Tool'],
            ['name' => 'Kit'],
            ['name' => 'Other']
        ];
        $discounts = [
            [
                'name' => 'Extra Ridiculous Deal',
                'amount' => 99999999,
                'type' => 'Rp'
            ],
            [
                'name' => 'Crazy Deal',
                'amount' => 50,
                'type' => '%'
            ],
            [
                'name' => 'Lunatic Deal',
                'amount' => 100000,
                'type' => 'Rp'
            ],
            [
                'name' => 'Humongous Deal',
                'amount' => 70,
                'type' => '%'
            ],
            [
                'name' => 'Lunar Year Deal',
                'amount' => 10,
                'type' => '%'
            ],
            [
                'name' => 'Ramadhan Deal',
                'amount' => 1000000,
                'type' => 'Rp'
            ],
            [
                'name' => 'Festival Deal',
                'amount' => 15,
                'type' => '%'
            ],
            [
                'name' => 'Christmas Deal',
                'amount' => 150000,
                'type' => 'Rp'
            ],
            [
                'name' => 'Best Deal',
                'amount' => 20,
                'type' => '%'
            ],
            [
                'name' => 'Extra Deal',
                'amount' => 500000,
                'type' => 'Rp'
            ],
            [
                'name' => 'Serious Deal',
                'amount' => 1400000,
                'type' => 'Rp'
            ],
            [
                'name' => 'Fun Deal',
                'amount' => 35,
                'type' => '%'
            ],
        ];
        $taxes = [
            [
                'name' => 'PPN',
                'amount' => 11
            ],
            [
                'name' => 'Product Tax',
                'amount' => 3
            ],
            [
                'name' => 'Application Tax',
                'amount' => 2
            ],
            [
                'name' => 'Shipping Tax',
                'amount' => 4
            ],
            [
                'name' => 'Excise Tax',
                'amount' => 1
            ],
            [
                'name' => 'Order Tax',
                'amount' => 6
            ],
            [
                'name' => 'International Tax',
                'amount' => 7
            ],
            [
                'name' => 'National Tax',
                'amount' => 5
            ],
            [
                'name' => 'Copyright Tax',
                'amount' => 9
            ],
            [
                'name' => 'Paten Tax',
                'amount' => 4
            ],
            [
                'name' => 'Black Tax',
                'amount' => 1
            ]
        ];
        $gratuities = [
            [
                'name' => 'Credit Card Change',
                'amount' => 5
            ],
            [
                'name' => 'Deliver Service',
                'amount' => 8
            ],
            [
                'name' => 'Tip Change',
                'amount' => 1
            ],
            [
                'name' => 'Go Food',
                'amount' => 13
            ],
            [
                'name' => 'Grab Food',
                'amount' => 15
            ],
            [
                'name' => 'Payment Service',
                'amount' => 10
            ],
            [
                'name' => 'Container Service',
                'amount' => 9
            ],
            [
                'name' => 'Box Service',
                'amount' => 5
            ],
            [
                'name' => 'Package Service',
                'amount' => 5
            ],
            [
                'name' => 'Admin Change',
                'amount' => 7
            ],
            [
                'name' => 'Shipping Service',
                'amount' => 10
            ]
        ];
        $brands = [
            [
                'name' => 'Google'
            ],
            [
                'name' => 'MSI'
            ],
            [
                'name' => 'Samsung'
            ],
            [
                'name' => 'Intel'
            ],
            [
                'name' => 'Acer'
            ],
            [
                'name' => 'NVDIA'
            ],
            [
                'name' => 'Apple'
            ],
            [
                'name' => 'Asus'
            ],
            [
                'name' => 'Tokopedia'
            ],
            [
                'name' => 'Gojek'
            ],
            [
                'name' => 'Yada Ekidanta'
            ]
        ];
        foreach ($categories as $category) {
            ItemCategory::create($category);
        }
        foreach ($discounts as $discount) {
            Discount::create($discount);
        }
        foreach ($taxes as $tax) {
            Tax::create($tax);
        }
        foreach ($gratuities as $gratuity) {
            Gratuity::create($gratuity);
        }
        foreach ($brands as $brand) {
            Brand::create($brand);
        }

        SalesType::create([
            'name' => 'Dine in',
            'status' => 'Active',
            'gratuity_applied' => [1, 2, 3, 4, 5],
        ]);
        SalesType::create([
            'name' => 'Dine out',
            'status' => 'Active',
            'gratuity_applied' => [3, 4, 5],
        ]);
        SalesType::create([
            'name' => 'Take away',
            'status' => 'Active',
            'gratuity_applied' => [2, 4, 5],
        ]);
    }
}
